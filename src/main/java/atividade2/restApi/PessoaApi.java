package atividade2.restApi;

import java.util.List;

import atividade2.dao.PessoaDAO;
import atividade2.domain.Pessoa;
import org.springframework.data.web.JsonPath;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author WilliamFernandoMende
 */
@RestController
public class PessoaApi {

    private PessoaDAO pessoaDAO= new PessoaDAO();

    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas(){
        return pessoaDAO.findAll();
    }


    @GetMapping("/id/{codigo}")
    public String obterPessoaId(@PathVariable int codigo){
        return pessoaDAO.findPersonById(codigo);
    }

    @GetMapping("/nome/{name}")
    public String obterPessoaNome(@PathVariable String name) {
        return pessoaDAO.findPersonByFirstName(name);
    }

    @GetMapping("/ano/{year}")
    public String obterPessoaAno(@PathVariable int year) {
        return pessoaDAO.findPersonByBirthYear(year);
    }

}

package atividade2.dao;

import atividade2.domain.Pessoa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 *
 * @author WilliamFernandoMende
 */
public class PessoaDAO {
    public final String DRIVER = "com.mysql.jdbc.Driver";
    public final String URL = "jdbc:mysql://localhost:3306/dbpessoas";
    public final String USER = "root";
    public final String PASSWORD = "";

    public PessoaDAO(){
        try{
            Class.forName(DRIVER);
        }catch(ClassNotFoundException e){
            e.printStackTrace();
            System.out.println("Driver do banco de dados não encontrado");
        }
    }

    public List<Pessoa> findAll(){
        final String sql = "SELECT * FROM PESSOA";
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados");
        }
        return pessoas;
    }

    public String findPersonByFirstName(String name){
        final String sql = "SELECT * FROM PESSOA WHERE nome LIKE '" + name + "%'";
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados H2");
        }
        if (pessoas.size() >= 1) {
            return pessoas.toString();
        } else {
            System.out.println("Vazio");
            return "Ninguem com esse nome";

        }
    }


    public List <Pessoa> findPersonByName(String name){
        final String sql = "SELECT * FROM PESSOA WHERE nome = "+ name;
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados");
        }
        return pessoas;
    }

    public String findPersonByBirthYear(int year){
        final String sql = "select * from pessoa where year(Data_Nascimento) >" + year;
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados");
        }
        if (pessoas.size() >= 1) {
            return pessoas.toString();
        } else {
            System.out.println("Vazio");
            return "Ninguem nascido a partir dessa data";

        }
    }


    public String findPersonById(int codigo){
        final String sql = "SELECT * FROM PESSOA WHERE codigo = "+ codigo;
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try( Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()){
            while(rs.next()){
                pessoas.add(pessoaMap(rs));
            }
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("Falha na conexao com o banco de dados");
        }
        if (pessoas.size() >=1){
            return pessoas.toString();
        }
        else{
            System.out.println("Vazio");
            return "Ninguem encontrado com esse ID";

        }

    }


    public Pessoa pessoaMap(ResultSet rs) throws SQLException {
        Pessoa pessoa = new Pessoa();
        pessoa.setId(rs.getInt("Codigo"));
        pessoa.setNome(rs.getString("Nome"));
        pessoa.setDocumento(rs.getString("Documento"));
        pessoa.setTelefone(rs.getString("Telefone"));
        pessoa.setData_nascimento(rs.getDate("Data_Nascimento"));
        return pessoa;
    }
}

